require("dotenv").config();

const express = require('express');

const cors = require('cors');

const sendSMS = require('./sendSMS');

const app = express();

const port = 3040;

const allowlist = process.env.ALLOW_LIST; // list of domains that can access the API via CORS

const corsOptionsDelegate = (req, callback) => {

  let corsOptions;

  let isDomainAllowed = allowlist.indexOf(req.header('Origin')) !== -1;

  if (isDomainAllowed) {
    // Enable CORS for this request
    corsOptions = { origin: true }
  } else {
    // Disable CORS for this request
    corsOptions = { origin: false }
  }
  callback(null, corsOptions)
}

app.use(cors(corsOptionsDelegate)); // middleware that can be used to enable CORS with various options.

app.use(express.urlencoded({ extended: true })); // parses incoming requests with urlencoded payloads

app.use(express.json()); // parses incoming requests with JSON payloads 

app.get('/', function (req, res) {

  res.send('Welcome to the SMS Microservice'); // welcome message

});

// Create notification endpoint
app.post('/', function (req, res) {

  const message = req.body;

  res.write('Message sent successfully!');

  res.send(sendSMS(message));

});

app.listen(port, () => {
  
  console.log(`Server running on port ${port}`);

});

module.exports = app;