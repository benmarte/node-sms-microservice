# SMS Notification Microservice

An express API microservice to send twilio SMS messages

## Prerequisites

- Twilio account
- Heroku account
- Docker
- DOcker Compose
- VScode

## Running locally with docker run

Create a `.env` file with the following values:

```
TWILIO_ACCOUNT_SID: "twilio sid"
TWILIO_AUTH_TOKEN: "twilio auth token"
TWILIO_FROM_PHONE: "twilio phone number"
ALLOW_LIST: "single or multiple domains to allow, values must be a single string separated by commas ie: 'http://localhost, http://localhost:3000'"
```

- build docker container via: `docker build -t node-sms-microservice .`
- run the container via: `docker run --env-file=.env -p 3040:3040 node-sms-microservice`

## Running locally with docker-compose

Edit `docker-compose.yml` environment variable values:

```
TWILIO_ACCOUNT_SID: "twilio sid"
TWILIO_AUTH_TOKEN: "twilio auth token"
TWILIO_FROM_PHONE: "twilio phone number"
ALLOW_LIST: "single or multiple domains to allow, values must be a single string separated by commas ie: 'http://localhost, http://localhost:3000'"
```

Then `docker-compose up`

## Running locally with npm or yarn

Create a .env file with the following environment variables and their respective values.

```
TWILIO_ACCOUNT_SID: "twilio sid"
TWILIO_AUTH_TOKEN: "twilio auth token"
TWILIO_FROM_PHONE: "twilio phone number"
ALLOW_LIST: "single or multiple domains to allow, values must be a single string separated by commas ie: 'http://localhost, http://localhost:3000'"
```

- install dependencies via: `yarn` or `npm i`
- Then run: `yarn start` or `npm start`

## How to debug in vscode with docker-compose

- Click the debug icon in VScode
- Ensure you have `Docker: Debug Node` selected in the drop down and click play
- VScode will build and open your browser with the app in debug mode
- Place a breakpoint anywhere in your code and refresh the app in your browser

## Debugging not working

If when you start a debugging session and it does not launch your app in the browser then you will have to do the following:

- Open a new terminal window
- Run `docker ps -a` identify your `nodesmsmicroservice` container port
- Navigate to `http://localhost:${container_port}` in this example my debugging port is: `49243` so I would need to enter: `http://localhost:49243` in my browser in order to start debugging.

![Debugging not working](/uploads/208e2a586c0595fff2c379ba75b621bc/sms.png)

## Gitlab Environment Variables

In order to have your app auto deploy on push to Heroku you need to create a few environment variables in Gitlab, these are:

- CI_REGISTRY - your gitlab registry url (usually prepend registry to your gitlab repo url)
- CI_REGISTRY_IMAGE - the name you want to use for your docker container
- CI_REGISTRY_PASSWORD - Setup a [personal access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html)
- CI_REGISTRY_USER - your gitlab username
- HEROKU_API_KEY - In Heroku go to your account settings and copy your API key or generate a new one
- HEROKU_APP - The name of your Heroku app

Ensure all of your Gitlab environment variables are **protected** and **masked** when possible to ensure the security of these.

![Gitlab ENV Vars](/uploads/68de9bd0cbb453e733340a95fdaa288d/image.png)

Now everytime you push changes to your repo Gitlab CI/CD will auto deploy to your heroku app.

## Heroku environment variables

When developing locally we use a `.env` file which has all the environment variable our app needs in order to run. Since we should never check in `.env` files in your repo we need to ensure these same environment variables exist on heroku in order for our app to work.

In Heroku go to your project settings and create environment variables for the following:

```
TWILIO_ACCOUNT_SID: "twilio sid"
TWILIO_AUTH_TOKEN: "twilio auth token"
TWILIO_FROM_PHONE: "twilio phone number"
ALLOW_LIST: "single or multiple domains to allow, values must be a single string separated by commas ie: 'http://localhost, http://localhost:3000'"
```

![image](/uploads/53e7ec452c515d09542a5452a918f0d4/Screenshot_2021-04-13_112717.png)

Once you have created these environement variables your app should be working in Heroku on your next push.
