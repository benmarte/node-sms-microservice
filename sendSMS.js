require('dotenv').config();
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const from = process.env.TWILIO_FROM_PHONE
const client = require('twilio')(accountSid, authToken);

const sendSMS = (message) => {
  // Download the helper library from https://www.twilio.com/docs/node/install
  // Your Account Sid and Auth Token from twilio.com/console
  // and set the environment variables. See http://twil.io/secure

  const { body, to } = message; // destructure params from message object here

  client.messages
    .create({
      body,
      from,
      to
    })
    .then(message => console.log(message.sid));
}

module.exports = sendSMS;