FROM node:12.18-alpine
ENV NODE_ENV=production
WORKDIR /opt/node-sms-microservice
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]
RUN npm install --production --silent && mv node_modules ../
COPY . .
EXPOSE 3040
CMD ["npm", "start"]
